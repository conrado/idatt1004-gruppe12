#!/usr/bin/env pybricks-micropython

"""
Example LEGO® MINDSTORMS® EV3 Tank Bot Program
----------------------------------------------

This program requires LEGO® EV3 MicroPython v2.0.
Download: https://education.lego.com/en-us/support/mindstorms-ev3/python-for-ev3

Building instructions can be found at:
https://education.lego.com/en-us/support/mindstorms-ev3/building-instructions#building-expansion
"""

from pybricks.hubs import EV3Brick
from pybricks.ev3devices import Motor, GyroSensor, UltrasonicSensor
from pybricks.parameters import Port, Direction, Button
from pybricks.tools import wait
from pybricks.robotics import DriveBase

ev3 = EV3Brick()

left_motor = Motor(Port.D, Direction.COUNTERCLOCKWISE)
right_motor = Motor(Port.B, Direction.COUNTERCLOCKWISE)
bridge_motor = Motor(Port.A, Direction.COUNTERCLOCKWISE)
# The wheel diameter of the Tank Bot is about 54 mm.
WHEEL_DIAMETER = 54

# The axle track is the distance between the centers of each of the
# wheels.  This is about 200 mm for the Tank Bot.
AXLE_TRACK = 200

robot = DriveBase(left_motor, right_motor, WHEEL_DIAMETER, AXLE_TRACK)

gyro_sensor = GyroSensor(Port.S4)
sonic_sensor = UltrasonicSensor(Port.S1)

# Initialize the steering and overshoot variables.
steering = 60
overshoot = 5


def turn180():
    gyro_sensor.reset_angle(0)

    robot.drive(0, steering)

    while gyro_sensor.angle() < 180 - overshoot:
        wait(1)
    robot.drive(0, 0)
    wait(1000)

def bridgedown():
    bridge_motor.run_angle(100, 90)
    wait(5000)

def bridgeup():
    bridge_motor.run_angle(100, -90)
    wait(5000)


while True:
    robot.drive(-100, 0)
    
    if sonic_sensor.distance() > 100:
        robot.straight(-100)
        turn180()
        bridgedown()

        turn180()
        bridgeup()

