#!/usr/bin/env pybricks-micropython

"""
Example LEGO® MINDSTORMS® EV3 Robot Educator Driving Base Program
-----------------------------------------------------------------

This program requires LEGO® EV3 MicroPython v2.0.
Download: https://education.lego.com/en-us/support/mindstorms-ev3/python-for-ev3

Building instructions can be found at:
https://education.lego.com/en-us/support/mindstorms-ev3/building-instructions#robot
"""

from pybricks.hubs import EV3Brick
from pybricks.ev3devices import Motor
from pybricks.parameters import Port
from pybricks.robotics import DriveBase

# Initialize the EV3 Brick.
ev3 = EV3Brick()

# Initialize the motors.
left_motor = Motor(Port.A)
right_motor = Motor(Port.B)

# Initialize the drive base.
robot = DriveBase(left_motor, right_motor, wheel_diameter=56, axle_track=150)

# Go forward and backwards for one meter.
ev3.screen.print("Hello World!")

robot.settings(straight_speed=100, straight_acceleration=50, turn_rate=45, turn_acceleration=45)

robot.straight(-100)
robot.turn(20)
robot.straight(-100)
robot.turn(20)
robot.straight(-100)
robot.turn(20)
robot.straight(-100)
robot.turn(20)

ev3.speaker.say("Bro i gotta take a shit")
